// alert("Hello Batch 224

// querySelector() is a method that can be used to select a specific element from our document.
console.log(document.querySelector("#txt-first-name"));

// document refers to thw whole page
console.log(document);

/*
	Alternative ways to access HTML elements. This is what we can use aside from the querySelector()

	document.getElementById("txt-first-name")
	document.getElementsByClass("txt-first-name")
	document.getElementsByTagName("input")
*/

// Example:
// getElementById() is similar to the querySelector()
console.log(document.getElementById("txt-first-name"));

// getElementByClassName() - Method returns a collection of elements with a specified class name/s
// It returns an HTMLCollection and property is read-only.
console.log(document.getElementsByClassName("txt-inputs"));

// getElementByTagName() - methods return a collection of all elements with a specified tag name/s
// It returns an HTMLCollection and property is read-only
console.log(document.getElementsByTagName("input"));

// Event and Event Listeners

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
console.log(txtFirstName);
console.log(spanFullName);

/*

		Event
				ex: click, hover, keypress and many other events.

		Event Listeners
				Allows us to let our user/s interact with our page. With each click
				or hover there is an event which triggers a function/task

		Syntax:
				selectedElement.addEventListener("event", function);

*/ 

// txtFirstName.addEventListener("keyup", (event) => {

// 	spanFullName.innerHTML = txtFirstName.value

// });

/*
		.innerHTML - is a property of an element which considers all the children of the selected
		element as a string.

		.value - this property contains the value of an element.

*/ 

// Another way to write the code for event handling
// txtFirstName.addEventListener("keyup", printFirstName);

// function printFirstName(event) {
// 	spanFullName.innerHTML = txtFirstName.value
// };

// We can add multiple events in an element
txtFirstName.addEventListener("keyup", (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})

/*
	The "event" argument contains the information on the triggered event.
	The "event.target" contains the element where the event happened.
	the "event.target.value" gets the value of the input object (this is similar to txtFirstName.value).
*/

const labelFirstName = document.querySelector("#label-first-name");
console.log(labelFirstName);

labelFirstName.addEventListener("click", (e) => {
	console.log(e)
	console.log(e.target)
	alert("You clicked the First Name label.")
})

// txtLastName.addEventListener("keyup", printLastName);

// function printLastName(event) {
// 	spanFullName.innerHTML = txtLastName.value
// };


/*
const updateFullName = () =>{
	let firstName = txtFirstName.value
	let lastName = txtLastName.value

	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
}
*/

// ACTIVITY

txtFirstName.addEventListener("keyup", getFullName);
txtLastName.addEventListener("keyup", getFullName)
function getFullName(event){
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`

}